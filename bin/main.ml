let q9 = ref false
let u = ref 42;;

Arg.parse
  [
    ("-u", Arg.Set_int u, "Valeur par défaut de u0");
    ("-q9", Arg.Set q9, "Exécuter la Q9 ?");
  ]
  (Printf.printf "L'argument %s n'est pas reconnu.\n")
  ""

module M = DMEnigma.Make (struct
  include DMEnigma.Default

  let u0 = abs !u
end)

let u i = M.u.(i)
and v i j = DMEnigma.Default.v ~i ~j ~u:M.u
;;

Printf.printf "Q1\n==\na. %d, %d\nb. %d, %d\nc. %d, %d\n" (u 10) (v 10 10)
  (u 1000) (v 1000 20) (u 100000) (v 100000 30)

let pi1 = M.pi ~n:5 ~i:10
and pi2 = M.pi ~n:50 ~i:1000
and pi3 = M.pi ~n:500 ~i:100000
;;

Printf.printf "Q2\n==\na. %d, %d, %d\nb. %d, %d, %d\nc. %d, %d, %d\n" (pi1 0)
  (pi1 1)
  (pi1 (5 - 1))
  (pi2 0) (pi2 1)
  (pi2 (50 - 1))
  (pi3 0) (pi3 1)
  (pi3 (500 - 1))

let print_list = [%show: int list]
let carac n i = M.caracteristique ~n (M.pi ~n ~i);;

Printf.printf "Q3\n==\na. %s\nb. %s\nc. %s\n"
  (print_list (carac 5 10))
  (print_list (carac 50 1000))
  (print_list (carac 500 100000))

let print_couple = [%show: char * char * char]

let c m i =
  let _, s_rp, _, s_k = M.sigmas ~k:(M.k ~m ~i ~n:0) in
  let x = s_rp 'A' in
  let y, z = (M.sigma_b x, s_k 'A') in
  (x, y, z)
;;

Printf.printf "Q4\n==\na. %s\nb. %s\nc. %s\n"
  (print_couple (c 1 10))
  (print_couple (c 2 100))
  (print_couple (c 3 1000))

let print_couple = [%show: char * char * char]

let c m n i =
  let k = M.k ~m ~n ~i in
  let s_c, s_rp, s_pr, s_k = M.sigmas ~k in
  let x = s_c 'A' in
  let y, z = (s_pr @@ M.sigma_b @@ s_rp x, s_k 'A') in
  (x, y, z)
;;

Printf.printf "Q5\n==\na. %s\nb. %s\nc. %s\n"
  (print_couple (c 1 5 20))
  (print_couple (c 2 8 200))
  (print_couple (c 3 10 2000))

let rec incubate f acc i =
  if i = 0 then acc else incubate f (fun c -> f @@ acc c) (i - 1)

and print_array = [%show: char array]

and d m i k =
  let r, p, _ = M.k ~i ~m ~n:0 in
  incubate (M.delta_r r) (fun i -> i) k p
;;

Printf.printf "Q6\n==\na. %s\nb. %s\nc. %s\n"
  (print_array (d 3 30 100))
  (print_array (d 4 300 1000))
  (print_array (d 5 3000 10000))
;;

Printf.printf "Q7\n==\na. %s\nb. %s\nc. %s\n"
  (M.encrypt "MESSAGE" ~k:(M.k ~m:1 ~n:5 ~i:40))
  (M.encrypt "MESSAGE" ~k:(M.k ~m:2 ~n:8 ~i:400))
  (M.encrypt "MESSAGE" ~k:(M.k ~m:3 ~n:10 ~i:4000))

let print_list = [%show: int list];;

Printf.printf "Q8\n==\na. %s\nb. %s\nc. %s\n"
  (M.caracteristique ~n:26 (fun i ->
       int_of_char (M.s ~k:(M.k ~m:1 ~n:5 ~i:50) (char_of_int (i + 65))) - 65)
  |> print_list)
  (M.caracteristique ~n:26 (fun i ->
       int_of_char (M.s ~k:(M.k ~m:2 ~n:8 ~i:500) (char_of_int (i + 65))) - 65)
  |> print_list)
  (M.caracteristique ~n:26 (fun i ->
       int_of_char (M.s ~k:(M.k ~m:3 ~n:10 ~i:5000) (char_of_int (i + 65))) - 65)
  |> print_list)
;;

if !q9 then
  let print = [%show: int * (int array * char array) option] in
  let t = Sys.time () in
  let x = M.q9 ~m:1 ~j:10 in
  let _y = M.q9 ~m:2 ~j:100 in
  let _z = M.q9 ~m:3 ~j:1000 in
  Printf.printf "Q9\n==\na. %s\nb. %s\nc. %s (%fs)\n" (print x)
    (* (print y) *) "" (* (print z) *) ""
    (Sys.time () -. t)

let a k l i = M.crib_alignment ~m:(M.mu ~l ~i:(i + k)) (M.mu ~l:k ~i)
and print_couple = [%show: int * int]
;;

Printf.printf "Q10\n===\na. %s\nb. %s\nc. %s\n"
  (print_couple (a 20 100 50))
  (print_couple (a 50 1000 500))
  (print_couple (a 100 10000 5000))

let print = [%show: char * (char * int) list];;

Printf.printf "Q11\n===\na. %s\nb. %s\nc. %s\n"
  (print (M.menu ~k:10 ~i:50))
  (print (M.menu ~k:15 ~i:500))
  (print (M.menu ~k:20 ~i:5000))
