module M = DMEnigma.Make (DMEnigma.Default)

let print_list = [%show: int list]
let carac n i = M.caracteristique ~n (M.pi ~n ~i);;

Printf.printf "Q3\n==\na. %s\nb. %s\nc. %s\n"
  (print_list (carac 5 10))
  (print_list (carac 50 1000))
  (print_list (carac 500 100000))
