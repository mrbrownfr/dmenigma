module M = DMEnigma.Make (DMEnigma.Default)

let print_list = [%show: int list];;

Printf.printf "Q8\n==\na. %s\nb. %s\nc. %s\n"
  (M.caracteristique ~n:26 (fun i ->
       int_of_char (M.s ~k:(M.k ~m:1 ~n:5 ~i:50) (char_of_int (i + 65))) - 65)
  |> print_list)
  (M.caracteristique ~n:26 (fun i ->
       int_of_char (M.s ~k:(M.k ~m:2 ~n:8 ~i:500) (char_of_int (i + 65))) - 65)
  |> print_list)
  (M.caracteristique ~n:26 (fun i ->
       int_of_char (M.s ~k:(M.k ~m:3 ~n:10 ~i:5000) (char_of_int (i + 65))) - 65)
  |> print_list)
