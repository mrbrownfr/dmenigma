module M = DMEnigma.Make (DMEnigma.Default)

let print = [%show: char * (char * int) list];;

Printf.printf "Q11\n===\na. %s\nb. %s\nc. %s\n"
  (print (M.menu ~k:10 ~i:50))
  (print (M.menu ~k:15 ~i:500))
  (print (M.menu ~k:20 ~i:5000))
