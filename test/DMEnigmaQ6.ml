module M = DMEnigma.Make (DMEnigma.Default)

let rec incubate f acc i =
  if i = 0 then acc else incubate f (fun c -> f @@ acc c) (i - 1)

and print_array = [%show: char array]

and d m i k =
  let r, p, _ = M.k ~i ~m ~n:0 in
  incubate (M.delta_r r) (fun i -> i) k p
;;

Printf.printf "Q6\n==\na. %s\nb. %s\nc. %s\n"
  (print_array (d 3 30 100))
  (print_array (d 4 300 1000))
  (print_array (d 5 3000 10000))
