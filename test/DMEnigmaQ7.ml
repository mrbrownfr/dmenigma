module M = DMEnigma.Make (DMEnigma.Default);;

Printf.printf "Q7\n==\na. %s\nb. %s\nc. %s\n"
  (M.encrypt "MESSAGE" ~k:(M.k ~m:1 ~n:5 ~i:40))
  (M.encrypt "MESSAGE" ~k:(M.k ~m:2 ~n:8 ~i:400))
  (M.encrypt "MESSAGE" ~k:(M.k ~m:3 ~n:10 ~i:4000))
