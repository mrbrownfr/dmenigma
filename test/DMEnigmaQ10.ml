module M = DMEnigma.Make (DMEnigma.Default)

let a k l i = M.crib_alignment ~m:(M.mu ~l ~i:(i + k)) (M.mu ~l:k ~i)
and print_couple = [%show: int * int]
;;

Printf.printf "Q10\n===\na. %s\nb. %s\nc. %s\n"
  (print_couple (a 20 100 50))
  (print_couple (a 50 1000 500))
  (print_couple (a 100 10000 5000))
