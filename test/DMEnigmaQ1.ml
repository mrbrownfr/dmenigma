module M = DMEnigma.Make (DMEnigma.Default)

let u i = M.u.(i)
and v i j = DMEnigma.Default.v ~i ~j ~u:M.u
;;

Printf.printf "Q1\n==\na. %d, %d\nb. %d, %d\nc. %d, %d\n" (u 10) (v 10 10)
  (u 1000) (v 1000 20) (u 100000) (v 100000 30)
