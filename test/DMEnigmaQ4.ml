module M = DMEnigma.Make (DMEnigma.Default)

let print_couple = [%show: char * char * char]

let c m i =
  let _, s_rp, _, s_k = M.sigmas ~k:(M.k ~m ~i ~n:0) in
  let x = s_rp 'A' in
  let y, z = (M.sigma_b x, s_k 'A') in
  (x, y, z)
;;

Printf.printf "Q4\n==\na. %s\nb. %s\nc. %s\n"
  (print_couple (c 1 10))
  (print_couple (c 2 100))
  (print_couple (c 3 1000))
