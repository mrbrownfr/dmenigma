module M = DMEnigma.Make (DMEnigma.Default)

let print_couple = [%show: char * char * char]

let c m n i =
  let k = M.k ~m ~n ~i in
  let s_c, s_rp, s_pr, s_k = M.sigmas ~k in
  let x = s_c 'A' in
  let y, z = (s_pr @@ M.sigma_b @@ s_rp x, s_k 'A') in
  (x, y, z)
;;

Printf.printf "Q5\n==\na. %s\nb. %s\nc. %s\n"
  (print_couple (c 1 5 20))
  (print_couple (c 2 8 200))
  (print_couple (c 3 10 2000))
