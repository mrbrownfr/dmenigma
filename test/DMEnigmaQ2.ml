module M = DMEnigma.Make (DMEnigma.Default)

let pi1 = M.pi ~n:5 ~i:10
and pi2 = M.pi ~n:50 ~i:1000
and pi3 = M.pi ~n:500 ~i:100000
;;

Printf.printf "Q2\n==\na. %d, %d, %d\nb. %d, %d, %d\nc. %d, %d, %d\n" (pi1 0)
  (pi1 1)
  (pi1 (5 - 1))
  (pi2 0) (pi2 1)
  (pi2 (50 - 1))
  (pi3 0) (pi3 1)
  (pi3 (500 - 1))
