let m = int_of_float ((2. ** 20.) +. 7.)
let u0 = 42
let unext u = 2035 * u mod m
let v ~i ~j ~u = (j + 1) * u.(i + j) / m

let rho c = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".[(Char.code c - 65 + 1) mod 26]
and sigma1 c = "EKMFLGDQVZNTOWYHXUSPAIBRCJ".[Char.code c - 65]
and sigma2 c = "AJDKSIRUXBLHWTMCQGZNPYFVOE".[Char.code c - 65]
and sigma3 c = "BDFHJLCPRTXVZNYEIWGAKMUSQO".[Char.code c - 65]
and sigma4 c = "ESOVPZJAYQUIRHXLNFTGKDCMWB".[Char.code c - 65]
and sigma5 c = "VZBRGITYUPSDNHLXAWMJQOFECK".[Char.code c - 65]
and sigma_b c = "YRUHQSLDPXNGOKMIEBFZCWVJAT".[Char.code c - 65]
