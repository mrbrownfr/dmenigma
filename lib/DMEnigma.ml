(** Ce module permet d'exposer le constructeur du DM*)

module Make (Init : sig
  val u0 : int
  val unext : int -> int
  val v : i:int -> j:int -> u:int array -> int
  val rho : char -> char
  val sigma1 : char -> char
  val sigma2 : char -> char
  val sigma3 : char -> char
  val sigma4 : char -> char
  val sigma5 : char -> char
  val sigma_b : char -> char
end) =
struct
  let u =
    let a = Array.make 1_000_000 Init.u0 in
    for i = 1 to 1_000_000 - 1 do
      a.(i) <- Init.unext a.(i - 1)
    done;
    a

  let pi ~n ~i =
    let x = Array.make n 0 in
    for j = 0 to n - 1 do
      let vj = Init.v ~i ~j ~u in
      x.(j) <- x.(vj);
      x.(vj) <- j
    done;
    fun k -> x.(k)

  (** Utilisation d'un DFS *)
  let caracteristique ~n f =
    let visites = Array.make n false in
    let paths = ref [] in
    let rec visite u path_len =
      if not visites.(u) then (
        visites.(u) <- true;
        visite (f u) (path_len + 1))
      else if path_len > 0 then paths := path_len :: !paths
    in
    for k = 0 to n - 1 do
      if not visites.(k) then visite k 0
    done;
    List.fast_sort compare !paths

  (*
  Pour R, 5^m possibilités sont disponibles.
  Pour P, il y a 26^m possibilités.
  Pour C, il y a \sum_{j = 0}^{n-1} \binom{26-2j}{2} possibilités.
*)

  let rho = Init.rho
  and sigma1 = Init.sigma1
  and sigma2 = Init.sigma2
  and sigma3 = Init.sigma3
  and sigma4 = Init.sigma4
  and sigma5 = Init.sigma5
  and sigma_b = Init.sigma_b

  let k ~i ~m ~n =
    let psi k = k + 1 in
    let phi u = Char.chr (65 + u) in
    ( Array.init m (fun k ->
          let p = pi ~n:5 ~i in
          psi (p k)),
      Array.init m (fun k -> phi (u.(i + 5 + k) mod 26)),
      Array.init n (fun j ->
          let p = pi ~n:26 ~i:(i + 10) in
          (phi (p (2 * j)), phi (p ((2 * j) + 1)))) )

  let rho_inv c =
    let a = Array.init 26 (fun i -> char_of_int (i + 65)) in
    for i = 0 to 25 do
      a.(int_of_char (Init.rho (char_of_int (i + 65))) - 65) <-
        char_of_int (i + 65)
    done;
    a.(int_of_char c - 65)

  let sigmas ~k =
    let r, p, c = k in

    let intosigma i =
      match i with
      | 1 -> Init.sigma1
      | 2 -> Init.sigma2
      | 3 -> Init.sigma3
      | 4 -> Init.sigma4
      | 5 -> Init.sigma5
      | _ -> failwith "[ERREUR] sigmas/intosigma : Permutation non définie !"
    in
    let chartoint c = Char.code c - 65 in

    let sigma_c =
      let a = Array.init 26 (fun i -> Char.chr (i + 65)) in
      Array.iter
        (fun (c1, c2) ->
          a.(chartoint c1) <- c2;
          a.(chartoint c2) <- c1)
        c;
      fun c -> a.(chartoint c)
    in

    let sigma_rp =
      let perms = Array.map intosigma r in
      let rec incubate sigma i =
        if i = 0 then sigma
        else incubate (fun c -> rho_inv @@ sigma @@ Init.rho c) (i - 1)
      in
      Array.fold_right
        (fun f1 f2 c -> f1 @@ f2 c)
        (Array.mapi (fun i c -> incubate perms.(i) (chartoint c)) p)
        (fun i -> i)
    in

    let sigma_pr =
      let a = Array.init 26 (fun i -> Char.chr (i + 65)) in
      for i = 0 to 25 do
        a.(chartoint @@ sigma_rp @@ Char.chr (i + 65)) <- Char.chr (i + 65)
      done;
      fun c -> a.(Char.code c - 65)
    in

    let sigma_k c =
      sigma_c c |> sigma_rp |> Init.sigma_b |> sigma_pr |> sigma_c
    in

    (sigma_c, sigma_rp, sigma_pr, sigma_k)

  let delta_r r p =
    let p' = Array.copy p in
    let rotor_to_advancechar = function
      | 1 -> 'Q'
      | 2 -> 'E'
      | 3 -> 'V'
      | 4 -> 'J'
      | 5 -> 'Z'
      | _ ->
          failwith "[ERREUR] delta_r/rotor_to_advancechar : Rotor non défini !"
    in
    let advance = ref true in
    for i = Array.length p - 1 downto 0 do
      let c = if !advance then rho p'.(i) else p'.(i) in
      advance := rotor_to_advancechar r.(i) = p'.(i) && !advance;
      p'.(i) <- c
    done;
    p'

  let delta ~k =
    let r, p, c = k in
    (r, delta_r r p, c)

  let encrypt message ~k =
    let config = ref k in
    String.map
      (fun c ->
        config := delta ~k:!config;
        let _, _, _, sigma = sigmas ~k:!config in
        sigma c)
      message

  let s_hat ~k =
    let r, p, c = k in
    let rec incubate f t i = if i = 0 then t else incubate f (f t) (i - 1) in
    let _, s_rp, s_pr, _ = sigmas ~k
    and _, s_rp2, s_pr2, _ =
      let d = delta_r r in
      sigmas ~k:(r, incubate d p (Array.length p), c)
    in
    fun c -> s_pr2 @@ sigma_b @@ s_rp2 @@ s_pr @@ sigma_b @@ s_rp c

  let s ~k =
    let r, p, c = k in
    let rec incubate f t i = if i = 0 then t else incubate f (f t) (i - 1) in
    let s_c, s_rp, s_pr, _ = sigmas ~k
    and _, s_rp2, s_pr2, _ =
      sigmas ~k:(r, incubate (delta_r r) p (Array.length p), c)
    in
    fun c ->
      s_c @@ s_pr2 @@ Init.sigma_b @@ s_rp2 @@ s_pr @@ Init.sigma_b @@ s_rp
      @@ s_c c

  let caracteristique' f ~n =
    let rec aux l =
      match l with
      | [] -> l
      | h :: t -> h :: h :: aux t
    in
    aux (caracteristique f ~n)

  let q9 ~m ~j =
    let rs =
      let all_uniques l =
        let presences = Array.make 5 false in
        let rec aux list =
          match list with
          | [] -> true
          | h :: t ->
              if presences.(h - 1) then false
              else (
                presences.(h - 1) <- true;
                aux t)
        in
        aux l
      in
      List.filter_map
        (fun l -> if all_uniques l then Some (Array.of_list l) else None)
        (List.init
           (int_of_float (5. ** float_of_int m))
           (fun i ->
             List.init m (fun k ->
                 1 + (i / int_of_float (5. ** float_of_int (m - k - 1)) mod 5))))
    in
    let ps =
      List.init
        (int_of_float (26. ** float_of_int m))
        (fun i ->
          Array.init m (fun k ->
              i / int_of_float (26. ** float_of_int (m - k - 1)) mod 26
              |> ( + ) 65 |> char_of_int))
    in
    let cartesian l l' =
      List.concat_map (fun e -> List.map (fun e' -> (e, e')) l') l
    in
    let pis =
      Array.init m (fun i ->
          caracteristique' ~n:13 @@ pi ~n:13 ~i:((13 * i) + j))
    in
    let rec check_config i k =
      if i >= m then true
      else
        let c =
          caracteristique ~n:26 (fun j ->
              int_of_char (s_hat ~k (char_of_int (j + 65))) - 65)
        in
        if c = pis.(i) then check_config (i + 1) (delta ~k) else false
    in
    List.fold_left
      (fun (counter, smallest) ((r, p) as h) ->
        let result = check_config 0 (delta ~k:(r, p, [||])) in
        ( (if result then 1 else 0) + counter,
          if result && smallest = None then Some h else smallest ))
      (0, None) (cartesian rs ps)

  let mu ~l ~i =
    let phi u = Char.chr (65 + u) in
    List.init l (fun k -> phi (u.(i + k) mod 26))

  let crib_alignment z ~m =
    let z', m' = (Array.of_list z, Array.of_list m) in
    let len_z, len_m = (List.length z, List.length m) in
    let is = List.init len_z (fun i -> i) in
    let smallest, counter = (ref (-1), ref 0) in
    for i = 0 to len_m - len_z do
      if not (List.exists (fun k -> z'.(k) = m'.(i + k)) is) then (
        if !smallest = -1 then smallest := i;
        counter := !counter + 1)
    done;
    (!counter, !smallest)
  (* let rec aux crib message smallest counter current_align =
       match (crib, message) with
       | [], _ ->
           aux z message
             (if smallest > -1 then smallest else current_align - len_z)
             (counter + 1) (current_align + 1)
       | _, [] -> (counter, smallest)
       | [], _ :: t -> aux z t smallest counter current_align
       | h :: t, h' :: t' ->
           if h' = ' ' || h = h' then
             aux z t' smallest counter (current_align + 1)
           else aux t t' smallest counter (current_align + 1)
     in
     aux z m (-1) 0 0 *)

  let menu ~k ~i =
    let crib = mu ~l:k ~i and m = mu ~l:100 ~i:(i + k) in
    let _, d = crib_alignment crib ~m in
    let m' = Array.of_list m and z = Array.of_list crib in
    let graph = Array.make 26 [] in
    for i = 0 to k - 1 do
      graph.(int_of_char z.(i) - 65) <-
        (m'.(d + i), d + i) :: graph.(int_of_char z.(i) - 65);
      graph.(int_of_char m'.(d + i) - 65) <-
        (z.(i), d + i) :: graph.(int_of_char m'.(d + i) - 65)
    done;
    let _, (char_idx, adjacence) =
      Array.fold_left
        (fun (idx, ((_, l1) as acc)) l2 ->
          if List.compare_lengths l1 l2 < 0 then (idx + 1, (65 + idx, l2))
          else (idx + 1, acc))
        (0, (65, []))
        graph
    in
    (char_of_int char_idx, adjacence)
end

module Default = Default
