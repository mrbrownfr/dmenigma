(** Ce module permet d'exposer le constructeur du DM*)

(** Ce module vous permet de générer toutes les fonctions du DM juste en passant en paramètre un module qui contient les primitives nécessaires au DM
 *)
module Make : functor
  (_ : sig
     (** Ce module contient toutes les primitives nécessaires au bon démarrage du programme. Voir la signature pour plus de détails *)

     val m : int
     val u0 : int

     val unext : int -> int
     (** La formule vous permettant de générer {m u_{n+1}} en fonction de {m u_n} *)

     val v : i:int -> j:int -> u:int array -> int
     (**La formule permettant de générer chaque valeur de {m v} *)

     val rho : char -> char
     (** Permutation "de base" *)

     val sigma1 : char -> char
     (** Permutation du rotor 1 *)

     val sigma2 : char -> char
     (** Permutation du rotor 2 *)

     val sigma3 : char -> char
     (** Permutation du rotor 3 *)

     val sigma4 : char -> char
     (** Permutation du rotor 4 *)

     val sigma5 : char -> char
     (** Permutation du rotor 5 *)

     val sigma_b : char -> char
     (** Permutation du réflecteur *)
   end)
  -> sig
  val u : int array
  (** Tableau contenant tous les {m u_n,\ n \in [\![0; 1000000]\!] } *)

  (* Réexport des fonctions du module initalisateur *)
  val rho : char -> char
  val rho_inv : char -> char
  val sigma1 : char -> char
  val sigma2 : char -> char
  val sigma3 : char -> char
  val sigma4 : char -> char
  val sigma5 : char -> char
  val sigma_b : char -> char

  val pi : n:int -> i:int -> int -> int
  (** Génère la permutation {m \pi_{n,i} } comme décrite dans l'énoncé *)

  val caracteristique : n:int -> (int -> int) -> int list
  (** Détermine la caractéristique d'une permutation
      @param n Le cardinal de l'ensemble de départ de la permutation
      @return La caractéristique de la fonction, triée dans l'ordre croissant
  *)

  val k :
    i:int -> m:int -> n:int -> int array * char array * (char * char) array
  (** Génère la configuration {m K_i}, pour {m m} et {m n} donnés
      @param m Le nombre de rotors à choisir
      @param n Le nombre de câbles à brancher sur le tableau
      @return Dans l'ordre:
        - Le tableau {m R} des rotors choisis
        - Le tableau {m P} des positions de départ de chaque rotor (dans l'ordre de {m R})
        - Le tableau {m C} des connexions
  *)

  val sigmas :
    k:int array * char array * (char * char) array ->
    (char -> char) * (char -> char) * (char -> char) * (char -> char)
  (** Génère les permutations associées à une configuration de la machine
        @param k La configuration de la machine
        @return Dans l'ordre:
            - {m \sigma_C}: correspond à la permutation du tableau de connexions
            - {m \sigma_{RP}}: correspond à la permutation des rotors, de droite à gauche
            - {m \sigma_{PR} = \sigma_{RP}^{-1}}: correspond à la permutation des rotors de droite à gauche
            - {m \sigma_K}: correspond à la permutation totale effectuée par la machine *)

  val delta_r : int array -> char array -> char array
  val delta : k:int array * char array * 'a -> int array * char array * 'a

  val encrypt :
    string -> k:int array * char array * (char * char) array -> string
  (** Encrypte un message d'après une configuration donnée
      @param k La configuration à respecter pour encrypter le message
  *)

  val s_hat : k:int array * char array * (char * char) array -> char -> char
  val s : k:int array * char array * (char * char) array -> char -> char

  val caracteristique' : (int -> int) -> n:int -> int list
  (** Wrapper autour de {!caracteristique} qui double chaque élément de la caractéristique *)

  val q9 : m:int -> j:int -> int * (int array * char array) option
  val mu : l:int -> i:int -> char list

  val crib_alignment : char list -> m:char list -> int * int
  (** Calcule les alignements valides pour un crib pour un message donné.
      @param `m` Le message codé
      @return le couple contenant dans l'ordre le nombre d'laignements possibles ainsi que l'indice du premier alignement possible.
    *)

  val menu : k:int -> i:int -> char * (char * int) list
end

module Default : sig
  val m : int
  val u0 : int
  val unext : int -> int
  val v : i:int -> j:int -> u:int array -> int
  val rho : char -> char
  val sigma1 : char -> char
  val sigma2 : char -> char
  val sigma3 : char -> char
  val sigma4 : char -> char
  val sigma5 : char -> char
  val sigma_b : char -> char
end
